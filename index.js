const http = require("http");
const uuid = require('uuid');
const fs = require("fs");
const v4 = uuid.v4;
const port = 8000;

const server = http.createServer((request, response)=>{
    if(request.method === 'GET' && request.url === '/'){
        response.write("Welcome to http drill");
        response.end();
    }
    else if(request.method === 'GET' && request.url === '/html'){
        fs.readFile("./index.html", "utf-8", (error, html_data)=>{
            if(error){
                response.writeHead(404, "Not found");
                response.end();
            }else{
                response.writeHead(200, {'content-type' : 'html'})
                const response_html_data = html_data;
                response.end(response_html_data);
            }
        });
        
    }else if(request.method === 'GET' && request.url === '/json'){
        response.writeHead(200, {'content-type' : 'html'})
        const json_data = {
            "slideshow": {
              "author": "Yours Truly",
              "date": "date of publication",
              "slides": [
                {
                  "title": "Wake up to WonderWidgets!",
                  "type": "all"
                },
                {
                  "items": [
                    "Why <em>WonderWidgets</em> are great",
                    "Who <em>buys</em> WonderWidgets"
                  ],
                  "title": "Overview",
                  "type": "all"
                }
              ],
              "title": "Sample Slide Show"
            }
          };

        response.end(JSON.stringify(json_data));

    }else if(request.method === 'GET' && request.url === '/uuid'){
        response.writeHead(200, {'content-type' : 'html'})
        const uuid_data = {
            "uuid": v4()
          };
        response.end(JSON.stringify(uuid_data));

    }else if(request.method === 'GET' && request.url.includes('/status/')){
        const status = parseInt(request.url.split('/')[2]);

        switch(status){
            case 100 :
                response.writeHead(status);
                response.end(status.toString());
                break;
            case 200:
                response.writeHead(status);
                response.end(status.toString());
                break;
            case 300:
                response.writeHead(status);
                response.end(status.toString());
                break;
            case 400:
                response.writeHead(status);
                response.end(status.toString());
                break;
            case 500:
                response.writeHead(status);
                response.end(status.toString());
                break;
            default:
                response.writeHead(status);
                response.end(`${status} not found`);
        };

    }else if(request.method === 'GET' && request.url.includes('/delay/')){

        const delay = parseInt(request.url.split('/')[2]);
        setTimeout(() => {
            response.writeHead(200);
            response.end(`successful ran after ${delay} second`);
        }, delay*1000);
    }else{
        response.end("Method not found");
    }
});

server.listen(port, ()=>{
    console.log(`listening on ${port}`);
});